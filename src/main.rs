/*

Copyright (C) 2022 Nathan Harmon

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.

*/

extern crate clap;
use clap::{Arg, App};

fn main() {
    let matches = App::new("splt")
        .version("0.1.0")
        .author("Nathan Harmon")
        .about("Split text around a delimiter.")
        .arg(Arg::with_name("delimiter")
            .help("The pattern to split the text by.")
            .short("d")
            .value_name("DELIMITER")
            .takes_value(true))
        .arg(Arg::with_name("INPUT")
            .help("Text input to split.")
            .required(true)
            .index(1)).get_matches();

    let delimiter: &str = matches.value_of("delimiter").unwrap_or(" ");
    let input: &str = matches.value_of("INPUT").unwrap();

    input.split(delimiter).for_each(|section| println!("{}", section));
}
