# Splt

A shell command to split text around a delimiter.

## Examples

### Using Output From Another Program

`$ echo $PATH | xargs splt -d:`

Outputs:

```
/home/youruser/.local/bin
/usr/local/sbin
/usr/local/bin
/usr/sbin
/usr/bin
/sbin
/bin
```

### Splitting Existing Text

`$ splt "This is an example sentence."`

Outputs:

```
This
is
an
example
sentence.
```

## Usage

```
USAGE:
    splt [OPTIONS] <INPUT>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -d <DELIMITER>        The pattern to split the text by.

ARGS:
    <INPUT>    Text input to split.
```

## Building

Ensure you have Rust and Cargo installed.

Clone the repo: `$ https://gitlab.com/nathanharmon/splt.git`

`$ cd splt`

Build a Binary: `$ cargo build --release`

The release is at `splt/target/release/splt`

## License

Copyright (C) 2022 Nathan Harmon

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
